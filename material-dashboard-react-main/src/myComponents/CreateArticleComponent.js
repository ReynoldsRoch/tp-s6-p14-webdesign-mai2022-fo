// import axios from "axios";
import React, { useState } from "react";

// @mui material components

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
// import MDButton from "components/MDButton";
import Header from "layouts/profile/components/Header";
import MDInput from "components/MDInput";
import MDButton from "components/MDButton";
import { CKEditor } from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import axios from "axios";
import MDAlert from "components/MDAlert";

// import axios from "axios";

function CreateArticleComponent() {

    const [alert, setAlert] = useState(0);

    document.title = "CREATE ARTICLE";

    const [image, setImage] = useState([]);

    // editor
    let editorValue = "<p>Hello from CKEditor 5!</p>";

    const createArticle = () => {
        console.log(editorValue);
        console.log(image.name);

        const title = document.getElementById("titre").value;
        const resume = document.getElementById("resume").value;
        console.log(title);
        console.log(resume);

        const url = `http://localhost:8080/articles?titre=${title}&resume=${resume}&contenu=${editorValue}&image=${image.name}&idclient=${sessionStorage.getItem("id")}`;

        axios.post(url).then((res) => {
            console.log(res.data);

            if (res.data === 1) {
                setAlert(1);
            }
        })
    }

    return (
        <DashboardLayout>
            <DashboardNavbar />
            <MDBox pt={6} pb={3}>
                <Header>
                    <MDBox pt={2} px={2} lineHeight={1.25}>
                        <MDTypography variant="h6" fontWeight="medium">
                            ARTICLES
                        </MDTypography>
                        <MDBox mb={1}>
                            <MDTypography variant="button" color="text">
                                LET THE WORLD KNOW THE TRUTH
                            </MDTypography>

                            {alert === 1 ?
                                <MDAlert color="success" dismissible>Article created successfully!
                                    <MDTypography variant="button" color="text">
                                        Waiting for admin&apos;s prohibition.
                                    </MDTypography>
                                </MDAlert>
                                : null}
                        </MDBox>
                    </MDBox>

                    <MDBox component="form" role="form">
                        <MDBox pt={2} px={2}>
                            <MDInput
                                type="file"
                                id="image"
                                label="Image"
                                fullWidth
                                required
                                onChange={(event) => {
                                    // console.log(event.target.files[0]);
                                    setImage(event.target.files[0]);
                                }}
                            />
                            {image && image.length !== 0 && (
                                <MDBox pt={2} px={2}>
                                    <div>
                                        <img
                                            alt="not found"
                                            width="200px"
                                            src={URL.createObjectURL(image)}
                                        />
                                        <br />
                                    </div>
                                </MDBox>
                            )}
                        </MDBox>
                        <MDBox pt={2} px={2}>
                            <MDInput
                                type="text"
                                id="titre"
                                defaultValue="Title"
                                label="Title"
                                variant="standard"
                                fullWidth
                                required
                            />
                        </MDBox>
                        <MDBox pt={2} px={2}>
                            <MDInput
                                type="text"
                                defaultValue="Resume"
                                label="Resume"
                                id="resume"
                                fullWidth
                                required
                                multiline rows={5}
                            />
                        </MDBox>
                        <MDBox pt={2} px={2}>
                            <CKEditor
                                editor={ClassicEditor}
                                data="<p>Hello from CKEditor 5!</p>"
                                onReady={editor => {
                                    // You can store the "editor" and use when it is needed.
                                    console.log('Editor is ready to use!', editor.data);
                                    editorValue = editor.getData();
                                }}
                                onChange={(event, editor) => {
                                    const data = editor.getData();
                                    editorValue = data;
                                    console.log({ event, editor, data });
                                }}
                                onBlur={(event, editor) => {
                                    console.log('Blur.', editor);
                                    editorValue = editor.getData();
                                }}
                                onFocus={(event, editor) => {
                                    console.log('Focus.', editor);
                                    editorValue = editor.getData();
                                }}
                            />
                        </MDBox>
                        <MDBox pt={2} px={2}>
                            <MDButton onClick={createArticle} variant="gradient" color="info" fullWidth>
                                CREATE
                            </MDButton>
                        </MDBox>
                    </MDBox>
                </Header>

            </MDBox>
            <Footer />
        </DashboardLayout>
    );
}

export default CreateArticleComponent;