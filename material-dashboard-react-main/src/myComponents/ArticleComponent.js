import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

// @mui material components
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";

// Material Dashboard 2 React components
import MDBox from "components/MDBox";
import MDTypography from "components/MDTypography";

// Material Dashboard 2 React example components
import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";

function ArticleComponent() {
    const { id } = useParams();

    const [article, setArticle] = useState([]);
    function getArticle() {
        const url = `http://localhost:8080/articles/${id}`;
        axios.get(url).then((res) => {
            setArticle(res.data);
        })
    }

    document.title = article.titre;
    useEffect(() => {
        getArticle();
        console.log(article);
    }, [])

    return (
        <DashboardLayout>
            <DashboardNavbar />
            <MDBox pt={6} pb={3}>
                <Grid container spacing={6}>
                    <Grid item xs={12}>
                        <Card>
                            <MDBox
                                mx={2}
                                mt={-3}
                                py={3}
                                px={2}
                                variant="gradient"
                                bgColor="info"
                                borderRadius="lg"
                                coloredShadow="info"
                            >
                                <MDTypography variant="h1" color="white">
                                    {article.titre}
                                </MDTypography>
                            </MDBox><br />
                            <MDBox pt={3} mx={2}
                                mt={-3}
                                py={3}
                                px={2}
                            >
                                <MDTypography>
                                    <div dangerouslySetInnerHTML={{ __html: article.contenu }} />
                                    {/* {article.contenu} */}
                                </MDTypography>
                                <br />

                                <br />
                                <MDTypography variant="body1">
                                    Published : {article.datePublication}
                                </MDTypography>
                            </MDBox>
                        </Card>
                    </Grid>
                </Grid>
            </MDBox>
            <Footer />
        </DashboardLayout>
    );
}

export default ArticleComponent;