import MDTypography from "components/MDTypography";
import React, { useEffect, useState } from "react";

function ProfileHeader() {
    const [client, setClients] = useState([]);
    function getClient() {
        try {
            const url = `http://localhost:8080/clients/${sessionStorage.getItem("id")}`;

            const xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.send();

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 && xhr.status === 200) {
                    console.log(JSON.parse(xhr.responseText));
                    setClients(JSON.parse(xhr.responseText));
                    console.log(client);
                }
            };
        } catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getClient();
    }, []);
    return (
        <div>
            <div key={client.id}>
                <MDTypography variant="h5" fontWeight="medium">
                    {client.nom} {client.prenom}
                </MDTypography>
                <MDTypography variant="button" color="text" fontWeight="regular">
                    {client.mail}
                </MDTypography>
            </div>
        </div>
    );
}

export default ProfileHeader;
